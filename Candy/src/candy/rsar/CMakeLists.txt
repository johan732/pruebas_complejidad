add_library(rsar OBJECT
  SolverAdapter.h
  SolverAdapter.cc
  ARSolver.h
  ARSolver.cc
  Refinement.h
  Refinement.cc
  Heuristics.h
  Heuristics.cc
  ApproximationState.h
  ApproximationState.cc)
